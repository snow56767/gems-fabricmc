package info.snowrealm.gems;

import net.fabricmc.api.ClientModInitializer;

public class GemsClient implements ClientModInitializer
{
    @Override
    public void onInitializeClient()
    {
        System.out.println("Initializing custom models...");
        System.out.println("Custom models initialized!");
    }
}
