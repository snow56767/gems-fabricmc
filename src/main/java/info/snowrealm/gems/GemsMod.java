package info.snowrealm.gems;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.fabricmc.fabric.api.event.player.PlayerBlockBreakEvents;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.BuiltinRegistries;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.gen.GenerationStep;
import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.decorator.RangeDecoratorConfig;
import net.minecraft.world.gen.feature.ConfiguredFeature;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.heightprovider.UniformHeightProvider;

public class GemsMod implements ModInitializer {
    public static final String _MOD_ID = "gems";

    public static GemOre _rubyOreBlock = new GemOre(FabricBlockSettings.of(Material.STONE).requiresTool().strength(3.0F, 3.0F));
    public static GemOre _sapphireOreBlock = new GemOre(FabricBlockSettings.of(Material.STONE).requiresTool().strength(3.0F, 3.0F));
    public static GemOre _topazOreBlock = new GemOre(FabricBlockSettings.of(Material.STONE).requiresTool().strength(3.0F, 3.0F));

    public static GemBlockItem _rubyOreItem = new GemBlockItem(_rubyOreBlock, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS));
    public static GemBlockItem _sapphireOreItem = new GemBlockItem(_sapphireOreBlock, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS));
    public static GemBlockItem _topazOreItem = new GemBlockItem(_topazOreBlock, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS));

    public static GemItem _ruby = new GemItem(new FabricItemSettings().group(ItemGroup.MATERIALS));
    public static GemItem _sapphire = new GemItem(new FabricItemSettings().group(ItemGroup.MATERIALS));
    public static GemItem _topaz = new GemItem(new FabricItemSettings().group(ItemGroup.MATERIALS));

    public static GemOre _rubyBlock = new GemOre(FabricBlockSettings.of(Material.STONE).requiresTool().strength(9.0F, 250.0F));
    public static GemOre _sapphireBlock = new GemOre(FabricBlockSettings.of(Material.STONE).requiresTool().strength(9.0F, 250.0F));
    public static GemOre _topazBlock = new GemOre(FabricBlockSettings.of(Material.STONE).requiresTool().strength(8.0F, 225.0F));

    public static GemBlockItem _rubyBlockItem = new GemBlockItem(_rubyBlock, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS));
    public static GemBlockItem _sapphireBlockItem = new GemBlockItem(_sapphireBlock, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS));
    public static GemBlockItem _topazBlockItem = new GemBlockItem(_topazBlock, new FabricItemSettings().group(ItemGroup.BUILDING_BLOCKS));

    private static ConfiguredFeature<?, ?> _oreGeneration_Ruby = Feature.ORE.configure(new OreFeatureConfig(OreFeatureConfig.Rules.BASE_STONE_OVERWORLD, _rubyOreBlock.getDefaultState(), 3))
            .range(new RangeDecoratorConfig(UniformHeightProvider.create(YOffset.aboveBottom(5), YOffset.fixed(17))))
            .spreadHorizontally()
            .repeat(3);
    private static ConfiguredFeature<?, ?> _oreGeneration_Sapphire = Feature.ORE.configure(new OreFeatureConfig(OreFeatureConfig.Rules.BASE_STONE_OVERWORLD, _sapphireOreBlock.getDefaultState(), 4))
            .range(new RangeDecoratorConfig(UniformHeightProvider.create(YOffset.aboveBottom(5), YOffset.fixed(20))))
            .spreadHorizontally()
            .repeat(4);
    private static ConfiguredFeature<?, ?> _oreGeneration_Topaz = Feature.ORE.configure(new OreFeatureConfig(OreFeatureConfig.Rules.BASE_STONE_OVERWORLD, _topazOreBlock.getDefaultState(), 5))
            .range(new RangeDecoratorConfig(UniformHeightProvider.create(YOffset.aboveBottom(21), YOffset.fixed(32))))
            .spreadHorizontally()
            .repeat(5);

    @Override
    public void onInitialize()
    {
        System.out.println("Gems mod initializing...");

        // Register our blocks.
        this.RegisterBlocks();

        // Register block items.
        this.RegisterBlockItems();

        // Register block drops.
        this.RegisterItems();

        // Register our ore generation.
        this.RegisterOreGeneration();

        System.out.println("Gems mod initialized!");
    }

    private void RegisterBlocks()
    {
        System.out.println("Registering blocks...");
        
        _rubyOreBlock.SetIdentifier(_MOD_ID, "ruby_ore");
        if (!_rubyOreBlock.IsRegistered())  { _rubyOreBlock.RegisterSelf(); }

        _sapphireOreBlock.SetIdentifier(_MOD_ID, "sapphire_ore");
        if (!_sapphireOreBlock.IsRegistered())  { _sapphireOreBlock.RegisterSelf(); }

        _topazOreBlock.SetIdentifier(_MOD_ID, "topaz_ore");
        if (!_topazOreBlock.IsRegistered())  { _topazOreBlock.RegisterSelf(); }

        _rubyBlock.SetIdentifier(_MOD_ID, "ruby_block");
        if (!_rubyBlock.IsRegistered()) { _rubyBlock.RegisterSelf(); }

        _sapphireBlock.SetIdentifier(_MOD_ID, "sapphire_block");
        if (!_sapphireBlock.IsRegistered()) { _sapphireBlock.RegisterSelf(); }

        _topazBlock.SetIdentifier(_MOD_ID, "topaz_block");
        if (!_topazBlock.IsRegistered()) { _topazBlock.RegisterSelf(); }


        System.out.println("Successfully registered blocks!");
    }

    private void RegisterBlockItems()
    {
        System.out.println("Registering block items...");

        _rubyOreItem.SetIdentifier(_MOD_ID, "ruby_ore");
        if (!_rubyOreItem.IsRegistered()) { _rubyOreItem.RegisterSelf(); }

        _sapphireOreItem.SetIdentifier(_MOD_ID, "sapphire_ore");
        if (!_sapphireOreItem.IsRegistered()) { _sapphireOreItem.RegisterSelf(); }

        _topazOreItem.SetIdentifier(_MOD_ID, "topaz_ore");
        if (!_topazOreItem.IsRegistered()) { _topazOreItem.RegisterSelf(); }

        _rubyBlockItem.SetIdentifier(_MOD_ID, "ruby_block");
        if (!_rubyBlockItem.IsRegistered()) { _rubyBlockItem.RegisterSelf(); }

        _sapphireBlockItem.SetIdentifier(_MOD_ID, "sapphire_block");
        if (!_sapphireBlockItem.IsRegistered()) { _sapphireBlockItem.RegisterSelf(); }

        _topazBlockItem.SetIdentifier(_MOD_ID, "topaz_block");
        if (!_topazBlockItem.IsRegistered()) { _topazBlockItem.RegisterSelf(); }

        System.out.println("Successfully registered block items!");
    }

    private void RegisterItems()
    {
        System.out.println("Registering items...");

        _ruby.SetIdentifier(_MOD_ID, "ruby");
        if (!_ruby.IsRegistered())  { _ruby.RegisterSelf(); }

        _sapphire.SetIdentifier(_MOD_ID, "sapphire");
        if (!_sapphire.IsRegistered())  { _sapphire.RegisterSelf(); }

        _topaz.SetIdentifier(_MOD_ID, "topaz");
        if (!_topaz.IsRegistered())  { _topaz.RegisterSelf(); }

        System.out.println("Successfully registered items!");
    }

    private void RegisterOreGeneration()
    {
        System.out.println("Registering ore generation...");

        // Rubies.
        RegistryKey<ConfiguredFeature<?, ?>> overworldOre_ruby = RegistryKey.of(Registry.CONFIGURED_FEATURE_KEY, new Identifier("gems", "ore_generation_ruby"));
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, overworldOre_ruby.getValue(), _oreGeneration_Ruby);
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, overworldOre_ruby);

        // Sapphires
        RegistryKey<ConfiguredFeature<?, ?>> overworldOre_sapphire = RegistryKey.of(Registry.CONFIGURED_FEATURE_KEY, new Identifier("gems", "ore_generation_sapphire"));
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, overworldOre_sapphire.getValue(), _oreGeneration_Sapphire);
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, overworldOre_sapphire);

        // Topaz
        RegistryKey<ConfiguredFeature<?, ?>> overworldOre_topaz = RegistryKey.of(Registry.CONFIGURED_FEATURE_KEY, new Identifier("gems", "ore_generation_topaz"));
        Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, overworldOre_topaz.getValue(), _oreGeneration_Topaz);
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, overworldOre_topaz);

        System.out.println("Successfully registered ores!");
    }
}
