package info.snowrealm.gems;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.DefaultedRegistry;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class GemOre extends Block
{
    private Identifier _oreIdentifier;
    private final DefaultedRegistry<Block> _registryType = Registry.BLOCK;
    private Boolean _isRegistered = false;

    public GemOre(Settings settings)
    {
        super(settings);
    }

    public Identifier GetIdentifier()  { return this._oreIdentifier; }
    public void SetIdentifier(String namespace, String id)  { this.SetIdentifier(namespace + ":" + id); }
    public void SetIdentifier(String namespace_with_id) { this._oreIdentifier = new Identifier(namespace_with_id); }

    public DefaultedRegistry<Block> GetRegistrationType()
    {
        return this._registryType;
    }

    public Boolean IsRegistered()
    {
        return this._isRegistered;
    }

    public Boolean RegisterSelf()
    {
        this._isRegistered = (null != Registry.register(GetRegistrationType(), GetIdentifier(), this));
        return IsRegistered();
    }
}
