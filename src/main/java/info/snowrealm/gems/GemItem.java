package info.snowrealm.gems;

import net.minecraft.item.Item;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.DefaultedRegistry;
import net.minecraft.util.registry.Registry;

public class GemItem extends Item
{
    private Identifier _itemIdentifier;
    private final DefaultedRegistry<Item> _registryType = Registry.ITEM;
    private Boolean _isRegistered = false;

    public GemItem(Settings settings)
    {
        super(settings);
    }

    public Identifier GetIdentifier()  { return this._itemIdentifier; }
    public void SetIdentifier(String namespace, String id)  { this.SetIdentifier(namespace + ":" + id); }
    public void SetIdentifier(String namespace_with_id) { this._itemIdentifier = new Identifier(namespace_with_id); }

    public DefaultedRegistry<Item> GetRegistrationType()  { return this._registryType; }

    public Boolean IsRegistered() { return this._isRegistered; }

    public Boolean RegisterSelf()
    {
        this._isRegistered = (null != Registry.register(GetRegistrationType(), GetIdentifier(), this));
        return IsRegistered();
    }
}
