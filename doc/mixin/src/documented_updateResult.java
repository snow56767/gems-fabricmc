/*!
 * @file AnvilScreenHelper.java
 * @line 92 - 269
 */
public void updateResult()
{
    // Get the bae input stack (This is the stack to which the modification will apply).
    ItemStack baseItemStack = this.input.getStack(0);
    this.levelCost.set(1); // Set the level cost for this modification to 1 xp.
    
    int xpCostDelta = 0;
    int finalXpCost = 0;
    int levelCost = 0;
    
    // Check and see if we even have a stack in slot 1.
    if (baseItemStack.isEmpty())
    {
        // We don't, so set no output stack with no cost.
        this.output.setStack(0, ItemStack.EMPTY);
        this.levelCost.set(0);
    }
    else
    {
        ItemStack outputItemStack = baseItemStack.copy(); // Make a copy of the base item stack for output.
        ItemStack modifyItemStack = this.input.getStack(1); // Acquire the modifing item stack.

        // Get any prexisting enchantments (we don't want to wipe existing echantments!)
        Map<Enchantment, Integer> existingEnchantmentMap = EnchantmentHelper.get(outputItemStack);
        
        // Calculate an XP cost based off what we know thus far.
        int finalXpCost = finalXpCost + baseItemStack.getRepairCost() + (modifyItemStack.isEmpty() ? 0 : modifyItemStack.getRepairCost());
        
        // Here we set the repaired items usage to 0 (effectively repairing it completely)
        this.repairItemUsage = 0;

        // Check to see if the modifying items are present.
        if (!modifyItemStack.isEmpty())
        {
            // Check if the item in slot 2 is an EnchantedBookItem. 
            boolean modifyIsEnchanter = modifyItemStack.isOf(Items.ENCHANTED_BOOK) && !EnchantedBookItem.getEnchantmentNbt(modifyItemStack).isEmpty();
            
            // Begin calculation of damage for the new item.
            // It's not 100% clear to me how this all works and to be frank, it's not important for Gems (yet).
            int outputStackDamageDelta;
            int modifyStackDamageDelta;
            int tmpStackDamage;

            // Check and see if the item in slot 2 can repair slot 1 and if repair is needed.
            if (outputItemStack.isDamageable() && outputItemStack.getItem().canRepair(baseItemStack, modifyItemStack))
            {
                outputStackDamageDelta = Math.min(outputItemStack.getDamage(), outputItemStack.getMaxDamage() / 4);
                if (outputStackDamageDelta <= 0)
                {
                    this.output.setStack(0, ItemStack.EMPTY);
                    this.levelCost.set(0);
                    return;
                }

                for(modifyStackDamageDelta = 0; outputStackDamageDelta > 0 && modifyStackDamageDelta < modifyItemStack.getCount(); ++modifyStackDamageDelta)
                {
                    tmpStackDamage = outputItemStack.getDamage() - outputStackDamageDelta;
                    outputItemStack.setDamage(tmpStackDamage);
                    ++xpCostDelta;
                    outputStackDamageDelta = Math.min(outputItemStack.getDamage(), outputItemStack.getMaxDamage() / 4);
                }

                this.repairItemUsage = modifyStackDamageDelta;
            }
            else // Not repairable, but possibly enchantable?
            {
                // Check to see if slot 2 is an enchanter and if it's NOT the same item as slot 1 OR has no damage done to it yet.
                if (!modifyIsEnchanter && (!outputItemStack.isOf(modifyItemStack.getItem()) || !outputItemStack.isDamageable()))
                {
                    // Above didn't hold true, so set the output to nothing with no cost.
                    this.output.setStack(0, ItemStack.EMPTY);
                    this.levelCost.set(0);
                    return;
                }

                // Check to see if slot 2 is not enchanter and if slot1 is damaged.
                if (outputItemStack.isDamageable() && !modifyIsEnchanter)
                {
                    // Do a repair-style calculation.
                    outputStackDamageDelta = baseItemStack.getMaxDamage() - baseItemStack.getDamage();
                    modifyStackDamageDelta = modifyItemStack.getMaxDamage() - modifyItemStack.getDamage();
                    tmpStackDamage = modifyStackDamageDelta + outputItemStack.getMaxDamage() * 12 / 100;
                    int stackDamageInverse = outputStackDamageDelta + tmpStackDamage;
                    int finalStackDamage = outputItemStack.getMaxDamage() - stackDamageInverse;
                    if (finalStackDamage < 0)
                    {
                        finalStackDamage = 0;
                    }

                    if (finalStackDamage < outputItemStack.getDamage())
                    {
                        outputItemStack.setDamage(finalStackDamage);
                        xpCostDelta += 2;
                    }
                }

                // Grab the enchanter's enchantments for application to the target (slot1).
                Map<Enchantment, Integer> modifyEnchantmentMap = EnchantmentHelper.get(modifyItemStack);
                boolean enchantmentApplied = false; // A simple flag to see if the enchantment is/can be applied.
                boolean impossibleEnchantment = false; // A simple flag to track for impossible enchantments/states.

                // Grab an iterator to the enchanter's enchantment map.
                Iterator modifyEnchantMapIter = modifyEnchantmentMap.keySet().iterator();
                
                /* TODO: Attempt to write a better version of this because this is pretty damn dumb. Infinite loops like this are not a good idea and are very easy to abuse. */
                while(true) // This is just bad form, but ok. Loop indefinitely.
                {
                    Enchantment enchantment; // Storage for the referenced enchantment.

                    /* TODO: Experiment and see if this can be made into a FOR loop which will help prevent programmer errors regarding things like infinite loops.*/
                    do 
                    {
                        // Check to see if there is an item after the iterator.
                        if (!modifyEnchantMapIter.hasNext())
                        {
                            // Check for an impossible/insane state.
                            if (impossibleEnchantment && !enchantmentApplied)
                            {
                                // We've entered an impossible/insane state, so set the output to nothing with no cost.
                                this.output.setStack(0, ItemStack.EMPTY);
                                this.levelCost.set(0);
                                return;
                            }
                            break;
                        }

                        // Grab the currently referenced enchantment.
                        enchantment = (Enchantment)modifyEnchantMapIter.next();
                    } while(enchantment == null); // Run DO-WHILE loop so long as we don't have an enchantment.
                    /* END TODO BLOCK */

                    // Extact the existing enchantment level from the base item's enchantments.
                    int exitingEnchantmentLevel = (Integer)existingEnchantmentMap.getOrDefault(enchantment, 0);

                    // Extract the current enchantment level from the enchanter.
                    int modifyEnchantmentLevel = (Integer)modifyEnchantmentMap.get(enchantment);

                    // Increase the modification enchantment level just a little bit (if allowed).
                    modifyEnchantmentLevel = exitingEnchantmentLevel == modifyEnchantmentLevel ? modifyEnchantmentLevel + 1 : Math.max(modifyEnchantmentLevel, exitingEnchantmentLevel);
                    
                    // Check to see if the current enchantment is applicable.
                    boolean canEnchantmentApply = enchantment.isAcceptableItem(baseItemStack);
                    if (this.player.getAbilities().creativeMode || baseItemStack.isOf(Items.ENCHANTED_BOOK))
                    {
                        // SPECIAL CASE!
                        // If the player is in creative OR the enchanter is an EnchantedBookItem, apply the enchantment anyway.
                        canEnchantmentApply = true;
                    }

                    // Grab an iterator for the pre-existing enchantment map.
                    Iterator existingEnchantmentIter = existingEnchantmentMap.keySet().iterator();

                    // Loop while the pre-existing enchantment iterator is valid (and be valid upon next loop).
                    /* TODO: Investigate to see if this is necessary. A FOR loop might be a better option here, only for readability. */
                    while(existingEnchantmentIter.hasNext())
                    {
                        // Grab the pre-existing enchantment.
                        Enchantment enchantment2 = (Enchantment)existingEnchantmentIter.next();
                        
                        // Check to see if the two enchantments are the same OR if they can be combined on one item.
                        if (enchantment2 != enchantment && !enchantment.canCombine(enchantment2))
                        {
                            // Nope, these enchantments are not compatible.
                            canEnchantmentApply = false;
                            ++xpCostDelta; // <-- Possible variable misname. Variable was just called 'i', so not 100% clear what it's for.
                        }
                    }

                    // Check if the enchantment can be applied.
                    if (!canEnchantmentApply)
                    {
                        // It can't be, set an impossible/insane state flag.
                        impossibleEnchantment = true;
                    }
                    else
                    {
                        // It can be!
                        enchantmentApplied = true;

                        // Simple bounds checking, Make sure the new enchantment level is NOT above it's maximum. If it is, just set it to the maximum for that enchantment.
                        if (modifyEnchantmentLevel > enchantment.getMaxLevel())
                        {
                            modifyEnchantmentLevel = enchantment.getMaxLevel();
                        }

                        // Add the new enchantment to the existing enchantments for the output item.
                        existingEnchantmentMap.put(enchantment, modifyEnchantmentLevel);
                        
                        // Calculate the XP cost for the enchantment being applied.
                        int enchantXpCost = 0;
                        switch(enchantment.getRarity())
                        {
                            case COMMON:
                                enchantXpCost = 1;
                                break;
                            case UNCOMMON:
                                enchantXpCost = 2;
                                break;
                            case RARE:
                                enchantXpCost = 4;
                                break;
                            case VERY_RARE:
                                enchantXpCost = 8;
                        }

                        if (modifyIsEnchanter)
                        {
                            // If the modify stack is an enchanter (such as EnchantedBookItem), divide the XP cost in half, ensuring it never goes below a cost of 1.
                            enchantXpCost = Math.max(1, enchantXpCost / 2);
                        }

                        // Calculate the cost against the new enchantment level. This helps bring the cost up from a few XP to a few levels.
                        xpCostDelta += enchantXpCost * modifyEnchantmentLevel;
                        if (baseItemStack.getCount() > 1)
                        {
                            // If we have more than one item recieving the enchantment, hard set the enchantment cost to 40 XP.
                            xpCostDelta = 40;
                        }
                    }
                }
                /* END OF TODO BLOCK */
            }
        }

        // Check to see if there is NO custom name.
        if (StringUtils.isBlank(this.newItemName))
        {
            // Check to see if the base item HAS a custom name.
            if (baseItemStack.hasCustomName())
            {
                // It does, so update the XP cost and remove the custom name.
                levelCost = 1;
                xpCostDelta += levelCost;
                outputItemStack.removeCustomName();
            }
        }
        else if (!this.newItemName.equals(baseItemStack.getName().getString()))
        {
            // There IS a new custom name, so update the XP cost and set the new name.
            levelCost = 1;
            xpCostDelta += levelCost;
            outputItemStack.setCustomName(new LiteralText(this.newItemName));
        }

        // Update the XP cost on the UI.
        this.levelCost.set(finalXpCost + xpCostDelta);
        if (xpCostDelta <= 0)
        {
            // If the level cost is less than 0, output no items.
            outputItemStack = ItemStack.EMPTY;
        }

        if (levelCost == xpCostDelta && levelCost > 0 && this.levelCost.get() >= 40)
        {
            // If the level cost is greater than 40, hard set it to 39. (for some reason).
            this.levelCost.set(39);
        }

        if (this.levelCost.get() >= 40 && !this.player.getAbilities().creativeMode)
        {
            // If the level cost is greater than 40 and the player is NOT in creative, output nothing.
            outputItemStack = ItemStack.EMPTY;
        }

        // Check to see if there is an output stack.
        if (!outputItemStack.isEmpty())
        {
            // Get the output stack repair cost.
            int outputRepairCost = outputItemStack.getRepairCost();
            
            // Check to see if there are modification items AND the output repair cost is less than the modify stack repair cost.
            if (!modifyItemStack.isEmpty() && outputRepairCost < modifyItemStack.getRepairCost())
            {
                // Set the output repair cost to the cost to repair the modification item stack.
                outputRepairCost = modifyItemStack.getRepairCost();
            }

            // This isn't super clear to me since these variables where all just i, j, and k.
            // It seems that we're checking to make sure the level cost and cost delta are the same.
            if (levelCost != xpCostDelta || levelCost == 0)
            {
                // Get the next repair cost for the output stack.
                outputRepairCost = getNextCost(outputRepairCost);
            }

            // Set the repair cost for the output stack.
            outputItemStack.setRepairCost(outputRepairCost);

            // Set the enchantments for the output stack.
            EnchantmentHelper.set(existingEnchantmentMap, outputItemStack);
        }

        // Set the output stack.
        this.output.setStack(0, outputItemStack);

        // Update the UI.
        this.sendContentUpdates();
    }
}