# Updating an exported model to work with the mod.
## What you need
Be sure to collect the following information:
* The name of the item (e.g. "Topaz")
* The texture name (e.g. "topaz_face")

## Updating the model file
Now, take your exported model file, Example: <br />
**Exported Model**
```json
{
    "credit": "Made with Blockbench",
    "elements": [
        {
            "from": [7.125, 4, 7],
            "to": [8.375, 8, 10],
            "rotation": {"angle": 0, "axis": "y", "origin": [7.75, 6, 8.5]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1.25, 4], "texture": "#missing"},
                "east": {"uv": [0, 0, 3, 4], "texture": "#missing"},
                "south": {"uv": [0, 0, 1.25, 4], "texture": "#missing"},
                "west": {"uv": [0, 0, 3, 4], "texture": "#missing"},
                "up": {"uv": [0, 0, 1.25, 3], "texture": "#missing"},
                "down": {"uv": [0, 0, 1.25, 3], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 4.325, 6.62],
            "to": [8.25, 7.625, 10.42],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.52]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 3.3], "rotation": 180, "texture": "#missing"},
                "east": {"uv": [0, 0, 3.3, 3.8], "rotation": 270, "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 3.3], "texture": "#missing"},
                "west": {"uv": [0, 0, 3.3, 3.8], "rotation": 90, "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 3.8], "rotation": 180, "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 3.8], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 3.625, 7.325],
            "to": [8.25, 8.325, 9.7],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.5125]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#missing"},
                "east": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#missing"},
                "west": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 2.375], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 2.375], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 3.82322, 6.82322],
            "to": [8.25, 4.82322, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.32322, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 7.12541, 9.20617],
            "to": [8.25, 8.12541, 10.20617],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.62541, 9.70617]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 3.8409, 9.20971],
            "to": [8.25, 4.8409, 10.20971],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.3409, 9.70971]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 7.11834, 6.82322],
            "to": [8.25, 8.11834, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.61834, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        }
    ]
}
```

First, we need to add some texture information. This is done by adding a new JSON object with the tag "textures", like so:
```json
    "textures": {
        "0": "gems:item/<texture_name>",
        "particle": "gems:item/<texture_name>"
    }
```

Be sure to replace `<texture_name>` with the name of the model's texture file (minus extension). For example:
```json
    "textures": {
        "0": "gems:item/topaz_face",
        "particle": "gems:item/topaz_face"
    }
```

Add, this to the root object under "credit", like so: <br />
**Modified Model**
```json
{
    "credit": "Made with Blockbench",
    "textures": {
        "0": "gems:item/topaz_face",
        "particle": "gems:item/topaz_face"
    },
    "elements": [
        {
            "from": [7.125, 4, 7],
            "to": [8.375, 8, 10],
            "rotation": {"angle": 0, "axis": "y", "origin": [7.75, 6, 8.5]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1.25, 4], "texture": "#missing"},
                "east": {"uv": [0, 0, 3, 4], "texture": "#missing"},
                "south": {"uv": [0, 0, 1.25, 4], "texture": "#missing"},
                "west": {"uv": [0, 0, 3, 4], "texture": "#missing"},
                "up": {"uv": [0, 0, 1.25, 3], "texture": "#missing"},
                "down": {"uv": [0, 0, 1.25, 3], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 4.325, 6.62],
            "to": [8.25, 7.625, 10.42],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.52]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 3.3], "rotation": 180, "texture": "#missing"},
                "east": {"uv": [0, 0, 3.3, 3.8], "rotation": 270, "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 3.3], "texture": "#missing"},
                "west": {"uv": [0, 0, 3.3, 3.8], "rotation": 90, "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 3.8], "rotation": 180, "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 3.8], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 3.625, 7.325],
            "to": [8.25, 8.325, 9.7],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.5125]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#missing"},
                "east": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#missing"},
                "west": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 2.375], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 2.375], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 3.82322, 6.82322],
            "to": [8.25, 4.82322, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.32322, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 7.12541, 9.20617],
            "to": [8.25, 8.12541, 10.20617],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.62541, 9.70617]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 3.8409, 9.20971],
            "to": [8.25, 4.8409, 10.20971],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.3409, 9.70971]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        },
        {
            "from": [7.25, 7.11834, 6.82322],
            "to": [8.25, 8.11834, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.61834, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#missing"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#missing"}
            }
        }
    ]
}
```

Next, we're going to update the `#missing` fields to use our new texture, which is texture `#0`, like so: <br />
**Textured Model**
```json
{
    "credit": "Made with Blockbench",
    "textures": {
        "0": "gems:item/topaz_face",
        "particle": "gems:item/topaz_face"
    },
    "elements": [
        {
            "from": [7.125, 4, 7],
            "to": [8.375, 8, 10],
            "rotation": {"angle": 0, "axis": "y", "origin": [7.75, 6, 8.5]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1.25, 4], "texture": "#0"},
                "east": {"uv": [0, 0, 3, 4], "texture": "#0"},
                "south": {"uv": [0, 0, 1.25, 4], "texture": "#0"},
                "west": {"uv": [0, 0, 3, 4], "texture": "#0"},
                "up": {"uv": [0, 0, 1.25, 3], "texture": "#0"},
                "down": {"uv": [0, 0, 1.25, 3], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 4.325, 6.62],
            "to": [8.25, 7.625, 10.42],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.52]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 3.3], "rotation": 180, "texture": "#0"},
                "east": {"uv": [0, 0, 3.3, 3.8], "rotation": 270, "texture": "#0"},
                "south": {"uv": [0, 0, 1, 3.3], "texture": "#0"},
                "west": {"uv": [0, 0, 3.3, 3.8], "rotation": 90, "texture": "#0"},
                "up": {"uv": [0, 0, 1, 3.8], "rotation": 180, "texture": "#0"},
                "down": {"uv": [0, 0, 1, 3.8], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.625, 7.325],
            "to": [8.25, 8.325, 9.7],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.5125]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#0"},
                "east": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#0"},
                "south": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#0"},
                "west": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#0"},
                "up": {"uv": [0, 0, 1, 2.375], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 2.375], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.82322, 6.82322],
            "to": [8.25, 4.82322, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.32322, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 7.12541, 9.20617],
            "to": [8.25, 8.12541, 10.20617],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.62541, 9.70617]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.8409, 9.20971],
            "to": [8.25, 4.8409, 10.20971],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.3409, 9.70971]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 7.11834, 6.82322],
            "to": [8.25, 8.11834, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.61834, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        }
    ]
}
```

After this is done, we want to add a JSON object that will adjust the model in the player's hand and item frames, so it displays correctly. For topaz, this object is:
```json
    "display": {
        "firstperson_righthand": {
            "translation": [0, 3, 0]
        },
        "firstperson_lefthand": {
            "translation": [0, 3, 0]
        },
        "thirdperson_righthand": {
            "translation": [0, 0, -1]
        },
        "thirdperson_lefthand": {
            "translation": [0, 0, -1]
        },
        "fixed": {
            "translation": [0, 2, -1],
            "scale": [2.5, 2.5, 2.5],
            "rotation": [0, 90, 0]
        }
    }
```

To break this down, many of the tags are self-explanatory, "firstperson_righthand" is the transform applied to the model when the player is holding it in their right hand when in first person. The "fixed" tag, is for item frames only.
Each one of these sub-objects can take 3 tags, each being an array of 3 items:
* translation
* rotation
* scale
(applied in that order)

The array has the format [X, Y, Z], so a value of [10, 5, -10] for "translation", will move the model 10 units on X, 5 units on Y, and negative 10 units on Z. Unfortunately, the [Minecraft Wiki on Models](https://minecraft.fandom.com/wiki/Model#Block_models) isn't super clear on which direction is which, so some experimentation is needed.

Simply insert this new "display" object into the root tag, after the "textures" tag, like so: <br />
**Display Adjusted Model**
```json
{
    "credit": "Made with Blockbench",
    "textures": {
        "0": "gems:item/topaz_face",
        "particle": "gems:item/topaz_face"
    },
    "display": {
        "firstperson_righthand": {
            "translation": [0, 3, 0]
        },
        "firstperson_lefthand": {
            "translation": [0, 3, 0]
        },
        "thirdperson_righthand": {
            "translation": [0, 0, -1]
        },
        "thirdperson_lefthand": {
            "translation": [0, 0, -1]
        },
        "fixed": {
            "translation": [0, 2, -1],
            "scale": [2.5, 2.5, 2.5],
            "rotation": [0, 90, 0]
        }
    },
    "elements": [
        {
            "from": [7.125, 4, 7],
            "to": [8.375, 8, 10],
            "rotation": {"angle": 0, "axis": "y", "origin": [7.75, 6, 8.5]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1.25, 4], "texture": "#0"},
                "east": {"uv": [0, 0, 3, 4], "texture": "#0"},
                "south": {"uv": [0, 0, 1.25, 4], "texture": "#0"},
                "west": {"uv": [0, 0, 3, 4], "texture": "#0"},
                "up": {"uv": [0, 0, 1.25, 3], "texture": "#0"},
                "down": {"uv": [0, 0, 1.25, 3], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 4.325, 6.62],
            "to": [8.25, 7.625, 10.42],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.52]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 3.3], "rotation": 180, "texture": "#0"},
                "east": {"uv": [0, 0, 3.3, 3.8], "rotation": 270, "texture": "#0"},
                "south": {"uv": [0, 0, 1, 3.3], "texture": "#0"},
                "west": {"uv": [0, 0, 3.3, 3.8], "rotation": 90, "texture": "#0"},
                "up": {"uv": [0, 0, 1, 3.8], "rotation": 180, "texture": "#0"},
                "down": {"uv": [0, 0, 1, 3.8], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.625, 7.325],
            "to": [8.25, 8.325, 9.7],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.5125]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#0"},
                "east": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#0"},
                "south": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#0"},
                "west": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#0"},
                "up": {"uv": [0, 0, 1, 2.375], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 2.375], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.82322, 6.82322],
            "to": [8.25, 4.82322, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.32322, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 7.12541, 9.20617],
            "to": [8.25, 8.12541, 10.20617],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.62541, 9.70617]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.8409, 9.20971],
            "to": [8.25, 4.8409, 10.20971],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.3409, 9.70971]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 7.11834, 6.82322],
            "to": [8.25, 8.11834, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.61834, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        }
    ]
}
```

Now you're \*technically* done! We add one last tag to the top of the root tag, and that's `"ambientocclusion": true,`, this is just makes it so the model has ambient occlusion applied to it. <br />
Final Model JSON should be: <br />
**Final Model**
```json
{
    "ambientocclusion": true,
    "credit": "Made with Blockbench",
    "textures": {
        "0": "gems:item/topaz_face",
        "particle": "gems:item/topaz_face"
    },
    "display": {
        "firstperson_righthand": {
            "translation": [0, 3, 0]
        },
        "firstperson_lefthand": {
            "translation": [0, 3, 0]
        },
        "thirdperson_righthand": {
            "translation": [0, 0, -1]
        },
        "thirdperson_lefthand": {
            "translation": [0, 0, -1]
        },
        "fixed": {
            "translation": [0, 2, -1],
            "scale": [2.5, 2.5, 2.5],
            "rotation": [0, 90, 0]
        }
    },
    "elements": [
        {
            "from": [7.125, 4, 7],
            "to": [8.375, 8, 10],
            "rotation": {"angle": 0, "axis": "y", "origin": [7.75, 6, 8.5]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1.25, 4], "texture": "#0"},
                "east": {"uv": [0, 0, 3, 4], "texture": "#0"},
                "south": {"uv": [0, 0, 1.25, 4], "texture": "#0"},
                "west": {"uv": [0, 0, 3, 4], "texture": "#0"},
                "up": {"uv": [0, 0, 1.25, 3], "texture": "#0"},
                "down": {"uv": [0, 0, 1.25, 3], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 4.325, 6.62],
            "to": [8.25, 7.625, 10.42],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.52]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 3.3], "rotation": 180, "texture": "#0"},
                "east": {"uv": [0, 0, 3.3, 3.8], "rotation": 270, "texture": "#0"},
                "south": {"uv": [0, 0, 1, 3.3], "texture": "#0"},
                "west": {"uv": [0, 0, 3.3, 3.8], "rotation": 90, "texture": "#0"},
                "up": {"uv": [0, 0, 1, 3.8], "rotation": 180, "texture": "#0"},
                "down": {"uv": [0, 0, 1, 3.8], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.625, 7.325],
            "to": [8.25, 8.325, 9.7],
            "rotation": {"angle": 0, "axis": "x", "origin": [7.75, 5.975, 8.5125]},
            "color": 6,
            "faces": {
                "north": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#0"},
                "east": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#0"},
                "south": {"uv": [0, 0, 1, 4.7], "rotation": 180, "texture": "#0"},
                "west": {"uv": [0, 0, 2.375, 4.7], "rotation": 180, "texture": "#0"},
                "up": {"uv": [0, 0, 1, 2.375], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 2.375], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.82322, 6.82322],
            "to": [8.25, 4.82322, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.32322, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 7.12541, 9.20617],
            "to": [8.25, 8.12541, 10.20617],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.62541, 9.70617]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 3.8409, 9.20971],
            "to": [8.25, 4.8409, 10.20971],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 4.3409, 9.70971]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        },
        {
            "from": [7.25, 7.11834, 6.82322],
            "to": [8.25, 8.11834, 7.82322],
            "rotation": {"angle": 45, "axis": "x", "origin": [7.75, 7.61834, 7.32322]},
            "color": 4,
            "faces": {
                "north": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "east": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "south": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "west": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "up": {"uv": [0, 0, 1, 1], "texture": "#0"},
                "down": {"uv": [0, 0, 1, 1], "texture": "#0"}
            }
        }
    ]
}
```

Simply place that in the "models" directory and you're good to go!

## DONE