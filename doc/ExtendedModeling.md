# Extended Minecraft Modeling
## Summary

We've added a more complex modeling and model material system to Minecraft to facilitate model complexity used by some gems. Notable examples of this are:
* Princess cut gems
* Trillion cut gems
* Drop cut gems
* Heart cut gems
* Octagon cut gems
* French cut gems
* Scissor cut gems
* Radiant cut gems
* Eight Corners cut gems

This page will go over how the system works and more importantly, HOW TO USE IT!

## Why Though?

As it turns out Minecraft JSON models are pretty limited:
* Models MUST use cubes
* Models Allow rotation on only ONE axis
* Models MUST be opaque (no transparency)

What this means is simply: you can't have complex shapes or even complex attributes! This means you can have a basic cube that is rotated on X, and only X. If you want to rotate that cube up onto it's pointy corner, not gonna happen. This of course, caused some issues with more complex gem cuts. We needed a way to allow us to put rotate on both X AND Z axis at minimum. We also wanted to add some transparency to the models, even if it was just based on the texture of the model. Minecraft, again, does NOT facilitate this for item models. So, we decided to create a system that DOES allow us to have both of these things and more. We decided to go with a system that allows even spheres should you be so inclined.

## System Limitations

As it turns out also, no system is a perfect "catch-all" or "do-everything" solution. So while this system allows for more complex shapes, it does NOT allow for model/vertex animations. So you gotta be OK with a static model. There is a plan to add animation, it's just not currently a thing.

\[_Insert Ticket for Animated Models_]

## Materials / Rendering

The new system also facilitates "model materials" which will allow transparency and potentially things such as "gloss". These materials are simple and do not get into anything too fancy such as specular mapping, transparency mapping, etc. These materials simply allow, much like regular minecraft, the use of a special Math function (such as Normal - or Gaussian - distribution) to calculate transparency/shine based off the "View Space". What this means is that the result is always calculated from the perspective of the camera, which makes it so the shine/transparency is always "facing" the viewer. This can create some awkward situations, but is needed due to the way Minecraft rendering works.

Onto the actual material files themselves. These files are simply JSON and allow for the following tags:
| JSON Tag     | Required | JSON Type | Description                                                                                 |
|:-------------|:--------:|:----------|:--------------------------------------------------------------------------------------------|
| name         | YES      | String    | Simply the name of the material (must be unique)                                            |
| type         | YES      | String    | Must be "MC-Extended-Material"                                                              |
| textures     | NO       | Object    | Simply specifies the model textures as a JSON object                                        |
| color        | NO       | List[3]   | Specifies the model's base color IF NO TEXTURE. If not present, model will be WHITE         |
| alpha        | NO       | Boolean   | Specifies if we should use the transparency from the texture. WILL OVERRIDE "transparency"! |
| transparency | NO       | Object    | Specifies the transparency function and members (see below).                                |
| gloss        | NO       | Boolean   | Specifies if the model has "gloss" or "shine"                                               |
| specular     | NO       | Object    | Specifies the specular/gloss function and members (see below)                               |
| author       | NO       | String    | Simply the author(s) as a string.                                                           |
| comment      | NO       | String    | Simply a comment for the model.                                                             |

Example material file:<br />
**Cube.mcexmat**<br />
```json
{
    "name": "Cube",
    "type": "MC-Extended-Material",
    "textures": {
        "0": "modid:extend/cube",
        "particle": "modid:extend/cube"
    },
    "alpha": false,
    "transparency": {
        "function": "normal",
        "variables": {
            "phi": 0.5,
            "mu": 255,
        }
    },
    "gloss": true,
    "specular": {
        "function": "normal",
        "variables": {
            "phi": 0.5,
            "mu": 255,
        }
    }
}
```

### Transparency and Specular functions

As mentioned above, the "transparency" and "specular" JSON objects specify a math function and it's members for application. These functions range from simple (flat) to complex (binomial and normal distribution). Each one provides a slightly different way to apply property to achieve the desired effect. For transparency, two limits are set: `0` (perfectly clear) and `1.0` (perfectly opaque). When the function runs with the given members (if any), any value it produces will be normalized to be between those two values. For specular, the limits are `0` (no shine) and `255` (perfect white shine).

For member specification, the system relies on the "Greek Letter Name" to determine what values goes to where in the equation. For a reference: [Greek Alphabet](https://en.wikipedia.org/wiki/Greek_alphabet#Sound_values)

The following functions are supported:
| Function    | Member List   | Description (Wiki Article) |
|:------------|:-------------:|:---------------------------|
| Flat        | `value`       | Simply set a value (_None_)|
| Linear      | `m`, `b`      | A simple linear falloff (https://en.wikipedia.org/wiki/Linear_function) |
| Exponential | `base`, `b`   | A simple exponential falloff (https://en.wikipedia.org/wiki/Exponential_function) |
| Quadratic   | `a`, `b`, `c` | A simple quadratic (square) falloff (https://en.wikipedia.org/wiki/Quadratic_function) |
| Logarithm   | `base`        | A simple logarithm falloff (https://en.wikipedia.org/wiki/Logarithm) |
| Binomial    | `n`, `p`      | A binomial distribution (https://en.wikipedia.org/wiki/Binomial_distribution) |
| Normal      | `phi`, `mu`   | A simple Normal/Gaussian distribution (https://en.wikipedia.org/wiki/Normal_distribution) |
| Zipfian     | `s`, `N`      | Zipfian's distribution (https://en.wikipedia.org/wiki/Zipf%27s_law) |

Some of these **will** produce similar results, so it's important to experiment!

_**TODO:** Put some example images here!_


## Creating Models

Currently, we recommend using a simple 3D modeling program such as Blender and exporting your finished model as an WaveFront OBJ then editing the result to have the correct values.

_**TODO:** Put a small tutorial on conversion from OBJ to mcexmod!_

Example model file:<br />
**Cube.mcexmod**<br />
```json
{
    "name": "Cube",
    "type": "MC-Extended-Model",
    "vertices": [
        [0, -1, 0],
        [1, -1, 0],
        [1, -1, 1],
        [0, -1, 1],
        [0, -1, 0],
        [0, 1, 0],
        [1, 1, 0],
        [1, 1, 1],
        [0, 1, 1],
        [0, 1, 0],
    ],
    "uvw": [
        [0, -1, 0],
        [1, -1, 0],
        [1, -1, 1],
        [0, -1, 1],
        [0, -1, 0],
        [0, 1, 0],
        [1, 1, 0],
        [1, 1, 1],
        [0, 1, 1],
        [0, 1, 0],
    ]
}
```

## Technical Specifications

| Property           | Value   |
|:-------------------|:-------:|
| File Type          | JSON    |
| Model Extension    | mcexmod |
| Material Extension | mcexmat |
| Pathing            | `<data>/<mod_id>/mcmodex/<model_name>/` |