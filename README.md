# Gems Mod for Minecraft
## Summary
This is a simple Minecraft mod that adds new gemstones which give special buffs, new crafting, and building blocks.

## Development
### Getting Started
First, grab a copy of the OpenJDK version 16: https://adoptopenjdk.net/ <br />
Then, clone this repository: `git clone https://gitlab.com/snow56767/gems-fabricmc.git` <br />
After that is done, you'll need to setup Fabric for development, generate the sources with the following command: <br />
Linux/Unix: `./gradlew genSources` <br />
Windows: `.\gradlew.bat genSources` <br />

After that is done, setup your IDE by following the Fabric [Setting up a mod development environment](https://fabricmc.net/wiki/tutorial:setup) wiki page. For an IDE, we recommend using [IntelliJ IDEA](https://www.jetbrains.com/idea/).

### Building, Cleaning, and Deploying 
To clean your project, simply run the following command (or have your IDE do it): <br />
Linux/Unix: `./gradlew clean` <br />
Windows: `.\gradlew.bat clean` <br />

To build the project: <br />
Linux/Unix: `./gradlew build` <br />
Windows: `.\gradlew.bat build` <br />

The `build` command will also deploy the mod. It'll end up in the `build/libs/` directory. The main modification file is called `gems-M.m.p.jar` where `M` is the major version, `m` is the minor version, and `p` is the patch.

## Art and Asset creation
### Textures
Texture work is fairly simple, we recommend [GIMP](https://www.gimp.org/) and/or [Krita](https://krita.org/en/). <br />
|  Format |  Resolution  |  Depth |  Mode |
|:--------|:------------:|-------:|:------|
| PNG     | 16px X 16px  | 32-bit | RGBA  |

All texture assets live under: `src/main/resources/assets/gems/textures/`. Block textures control what textures are shown on both the blocks in world AND blocks in hand (inventory). The "item" textures are a bit of a misnomer, they're actually the textures the are used on the item models. These textures are \*technically* UV mapped, but are not unwrapped as we don't care to unwrap the models and texture them one at a time.

### Models
All of our items are models. This means that instead of doing the default minecraft flat "3D-ish" texture, we have actual models to represent each gemstone. This also means that each model as a unique "cut". Please refer to the table and image beneath for reference as to what cuts/models are currently in use and which are available.

All the models are created using [BlockBench](https://web.blockbench.net/), which is a simple to use Minecraft modeling program. The models are created using the "Java Block/Item" template and are exported as "Java Block/Item". We don't worry about actually texturing the model in the application (you can if you want) and so we don't care to unwrap the UVs.

We also build all models with the "Front Face" of the gemstone facing "Negative X", or "West". All models are started on coordinates [7, 4, 7], and center around those. If you need an example, import either the Ruby, Sapphire, or Topaz models and take a look at how they're built.

When you export the model, be sure to save it to the appropriate JSON file under `src/main/resources/assets/gems/models/item/`. Also be sure to format it correctly and add the correct fields as needed. For an example, see [doc/ExampleModel.md](doc/ExampleModel.md)

After all is said an done and you have a working model with texturing and it correctly displays, be sure to update the table above and commit it.

| Gemstone Cut        | In use? | Item     |
|:--------------------|:-------:|:---------|
| Fine Round          | NO      |          |
| Princess (Small)    | NO      |          |
| Half Rose           | NO      |          |
| Pear                | YES     | Sapphire |
| Radiant             | NO      |          |
| Brilliant           | YES     | Diamond  |
| Cushion (Round)     | NO      |          |
| Square              | NO      |          |
| Emerald (Round)     | NO      |          |
| Ball                | NO      |          |
| Table Cut           | YES     | Topaz    |
| Cushion (Square)    | NO      |          |
| Heart               | NO      |          |
| Emerald (Square)    | YES     | Emerald  |
| Rectangular (Small) | NO      |          |
| Trillion            | NO      |          |
| Eight Corners       | NO      |          |
| Pampel              | NO      |          |
| Trapeze             | NO      |          |
| Drop                | NO      |          |
| Can                 | NO      |          |
| Cushion (Square)    | YES     | Ruby     |
| Rose                | NO      |          |
| Navette             | NO      |          |
| Oval                | NO      |          |
| Asscher             | NO      |          |
| Princess (Large)    | NO      |          |
| Rectangular (Large) | NO      |          |
| Octagon             | NO      |          |
| Pendeloque          | NO      |          |
| Scissor             | NO      |          |
| Step                | NO      |          |
| Ceylon              | NO      |          |
| French              | NO      |          |
| Round               | NO      |          |

![Gem Cuts Image](doc/gemstone_cuts.jpg "Gem Cuts")